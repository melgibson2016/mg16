-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Loomise aeg: Märts 21, 2016 kell 08:14 PL
-- Serveri versioon: 10.1.9-MariaDB
-- PHP versioon: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Andmebaas: `mg16`
--
CREATE DATABASE IF NOT EXISTS `mg16` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mg16`;

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `advertisement`
--

CREATE TABLE `advertisement` (
  `id` int(11) NOT NULL,
  `advertiser_user_id` int(11) DEFAULT NULL,
  `item_price` double DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `payment_type` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `description` text,
  `category_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Andmete tõmmistamine tabelile `advertisement`
--

INSERT INTO `advertisement` (`id`, `advertiser_user_id`, `item_price`, `amount`, `payment_type`, `location`, `description`, `category_id`) VALUES
(1, 1, 232, 2, 'raha', 'tartu', 'kirjeldus.kirjeldus.kirjeldus.kirjeldus.kirjeldus.kirjeldus.kirjeldus.kirjeldus.kirjeldus.', 1),
(2, 1, 23, 2323, '322323', '232332', '323322', 2),
(3, 1, 232, 32, '232', '3', '3', 1),
(4, 1, 87, 8, '78', '78', '78', 1);

-- --------------------------------------------------------

--
-- Sise-vaate struktuur `advertisement_view`
--
CREATE TABLE `advertisement_view` (
`advertisement_id` int(11)
,`advertiser_user_id` int(11)
,`item_price` double
,`amount` double
,`payment_type` varchar(45)
,`location` varchar(45)
,`description` text
,`category_id` int(11)
,`category_name` varchar(45)
,`firstname` varchar(45)
,`lastname` varchar(45)
,`email` varchar(45)
,`username` varchar(45)
);

-- --------------------------------------------------------

--
-- Sise-vaate struktuur `advertisement_view_1`
--
CREATE TABLE `advertisement_view_1` (
`id` int(11)
);

-- --------------------------------------------------------

--
-- Sise-vaate struktuur `advertisement_view_2`
--
CREATE TABLE `advertisement_view_2` (
`advertisement_id` int(11)
,`advertiser_user_id` int(11)
,`item_price` double
,`amount` double
,`payment_type` varchar(45)
,`location` varchar(45)
,`description` text
,`category_id` int(11)
,`category_name` varchar(45)
,`firstname` varchar(45)
,`lastname` varchar(45)
,`email` varchar(45)
,`username` varchar(45)
);

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Andmete tõmmistamine tabelile `category`
--

INSERT INTO `category` (`id`, `category_name`) VALUES
(1, 'Autod'),
(2, 'Elektroonika'),
(3, 'Ilu ja tervis'),
(4, 'Koolitarbed'),
(5, 'Loomad');

-- --------------------------------------------------------

--
-- Tabeli struktuur tabelile `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Andmete tõmmistamine tabelile `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `username`, `password`) VALUES
(1, 'Mihkel', 'Vilismäe', 'mihkel.vilismae@gmail.com', 'mihkel', NULL);

-- --------------------------------------------------------

--
-- Vaate struktuur `advertisement_view`
--
DROP TABLE IF EXISTS `advertisement_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `advertisement_view`  AS  select `ad`.`id` AS `advertisement_id`,`ad`.`advertiser_user_id` AS `advertiser_user_id`,`ad`.`item_price` AS `item_price`,`ad`.`amount` AS `amount`,`ad`.`payment_type` AS `payment_type`,`ad`.`location` AS `location`,`ad`.`description` AS `description`,`ad`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`u`.`firstname` AS `firstname`,`u`.`lastname` AS `lastname`,`u`.`email` AS `email`,`u`.`username` AS `username` from ((`advertisement` `ad` left join `category` `c` on((`ad`.`category_id` = `c`.`id`))) left join `user` `u` on((`ad`.`advertiser_user_id` = `u`.`id`))) ;

-- --------------------------------------------------------

--
-- Vaate struktuur `advertisement_view_1`
--
DROP TABLE IF EXISTS `advertisement_view_1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `advertisement_view_1`  AS  select `ad`.`id` AS `id` from ((`advertisement` `ad` left join `category` `c` on((`ad`.`category_id` = `c`.`id`))) left join `user` `u` on((`ad`.`advertiser_user_id` = `u`.`id`))) ;

-- --------------------------------------------------------

--
-- Vaate struktuur `advertisement_view_2`
--
DROP TABLE IF EXISTS `advertisement_view_2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `advertisement_view_2`  AS  select `ad`.`id` AS `advertisement_id`,`ad`.`advertiser_user_id` AS `advertiser_user_id`,`ad`.`item_price` AS `item_price`,`ad`.`amount` AS `amount`,`ad`.`payment_type` AS `payment_type`,`ad`.`location` AS `location`,`ad`.`description` AS `description`,`ad`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`u`.`firstname` AS `firstname`,`u`.`lastname` AS `lastname`,`u`.`email` AS `email`,`u`.`username` AS `username` from ((`advertisement` `ad` join `category` `c` on((`ad`.`category_id` = `c`.`id`))) join `user` `u` on((`ad`.`advertiser_user_id` = `u`.`id`))) ;

--
-- Indeksid tõmmistatud tabelitele
--

--
-- Indeksid tabelile `advertisement`
--
ALTER TABLE `advertisement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Advertisement_User1_idx` (`advertiser_user_id`),
  ADD KEY `fk_Advertisement_Category_idx` (`category_id`);

--
-- Indeksid tabelile `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indeksid tabelile `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT tõmmistatud tabelitele
--

--
-- AUTO_INCREMENT tabelile `advertisement`
--
ALTER TABLE `advertisement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT tabelile `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT tabelile `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Tõmmistatud tabelite piirangud
--

--
-- Piirangud tabelile `advertisement`
--
ALTER TABLE `advertisement`
  ADD CONSTRAINT `fk_Advertisement_Category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Advertisement_User1` FOREIGN KEY (`advertiser_user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
