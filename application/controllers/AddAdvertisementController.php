<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AddAdvertisementController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = ['categories' => $this->CategoryModel->getCategories()];


		if ($this->session->user['id'] && isset($_GET["pealkiri"]) && isset($_GET["hind"]) && isset($_GET["kogus"]) && isset($_GET["maksmine"]) && isset($_GET["asukoht"]) && isset($_GET["kirjeldus"]) && isset($_GET["kategooria"])) {
			$this->load->model('AdvertisementModel');
			$this->AdvertisementModel->addAdvertisement($this->session->user['id'], $_GET["hind"], $_GET["kogus"], $_GET["maksmine"], $_GET["asukoht"], $_GET["kirjeldus"], $_GET["kategooria"], $_GET['pealkiri']);
			redirect("/".getLanguage()."/Kategooria/".$_GET["kategooria"]);

		}

		$this->lang->load(array('add_advertisement', 'top_bar', 'welcome_text', 'header', 'category_list'), getLanguage());
		$this->load->view('add_advertisement', $data);
	}
}
