<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdvertisementController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = [];
		$this->lang->load(array('advertisement', 'top_bar', 'welcome_text', 'header'), getLanguage());
		$advertisementId = $this->uri->segment(3);
		$paymentInfo = $this->uri->segment(4);
		if ($paymentInfo) {
			$data['paymentSuccess'] = ($paymentInfo=='MakseOnnestus'?true:false);
		}
		$data['advertisement'] = $this->AdvertisementModel->getAdvertisement($advertisementId);
		$this->load->view('advertisement', $data);
//		print_r($this->db->query('show tables'));
	}

	public function bank() {
		$advertisementId = $this->uri->segment(3);
		$data = [];
		$data['advertisement'] = $this->AdvertisementModel->getAdvertisement($advertisementId);
		$this->lang->load(array('advertisement', 'top_bar', 'welcome_text', 'header'), getLanguage());
		$this->load->view('buy', $data);
	}
}
