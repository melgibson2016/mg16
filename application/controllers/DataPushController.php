<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataPushController extends CI_Controller
{
    // Kasutatud seda: http://www.howopensource.com/2014/12/introduction-to-server-sent-events/
    public function adminMessage() {
        $this->DataPushMessageModel->createMessage($this->input->post('message'));
        echo "Message sent!";
    }

    public function index() {
        /**
         * Server-side file.
         * This file is an infinitive loop. Seriously.
         * It gets the file data.txt's last-changed timestamp, checks if this is larger than the timestamp of the
         * AJAX-submitted timestamp (time of last ajax request), and if so, it sends back a JSON with the data from
         * data.txt (and a timestamp). If not, it waits for one seconds and then start the next while step.
         *
         * Note: This returns a JSON, containing the content of data.txt and the timestamp of the last data.txt change.
         * This timestamp is used by the client's JavaScript for the next request, so THIS server-side script here only
         * serves new content after the last file change. Sounds weird, but try it out, you'll get into it really fast!
         */
// set php runtime to unlimited
        set_time_limit(0);
        session_write_close();
//        fb($_POST);
//        if (!isset($_POST['timestamp'])) {
//            fb('pole timestampi');
////                die();
//        }
//        else {
//            fb('on timestamp');
//        }
//            $lastMessageTimestamp = isset($_POST['timestamp']) ? (int)$_POST['timestamp'] : 1;
        $lastMessageTimestamp = isset($_POST['timestamp']) ? (int)$_POST['timestamp'] : microtime(true);
// main loop
        while (true) {
            // if ajax request has send a timestamp, then $last_ajax_call = timestamp, else $last_ajax_call = null


            // PHP caches file data, like requesting the size of a file, by default. clearstatcache() clears that cache
            clearstatcache();
            // get timestamp of when file has been changed the last time

            // if no timestamp delivered via ajax or data.txt has been changed SINCE last ajax timestamp
            $message = $this->DataPushMessageModel->getFirstMessageAfterTimestamp($lastMessageTimestamp);

            if ($message) {
//            if ($lastMessageTimestamp == null || $message) {
                // get content of data.txt
//                $data = file_get_contents($data_source_file);
                // put data.txt's content and timestamp of last data.txt change into array
                $result = array(
                    'messageText' => $message['message_text'],
                    'senderUserId' => $message['sender_user_id'],
                    'messageTimestamp' => $message['message_timestamp']
                );
//            die("x");
                // encode to JSON, render the result (for AJAX)
                $json = json_encode($result);
                echo $json;
                // leave this loop step
                break;
            } else {
                // wait for 1 sec (not very sexy as this blocks the PHP/Apache process, but that's how it goes)
                sleep( 3 );
                continue;
            }
//            exit();
        }


    }

//    public function index()
//    {
//        set_time_limit(0);
//        header("Content-Type: text/event-stream");
//        header("Cache-Control: no-cache");
//        header("Connection: keep-alive");
//
//        $lastId = $_SERVER["HTTP_LAST_EVENT_ID"];
//        if (isset($lastId) && !empty($lastId) && is_numeric($lastId)) {
//            $lastId = intval($lastId);
//            $lastId++;
//        }
//
//        while (true) {
////            fb('uurib serverit');
//            $data = $this->DataPushMessageModel->getMessage();
//            if ($data) {
//                $messageText = $data[0]['message_text'];
//                $this->DataPushMessageModel->removeMessage($data[0]['id']);
//                $this->sendMessage($lastId, $messageText);
////                $this->sendMessage($lastId, "xxx");
//                $lastId++;
//            }
//            sleep(1);
//        }
//        session_write_close();
//    }

    function sendMessage($id, $data)
    {
        echo "id: $id\n";
        echo "data: $data\n\n";
        ob_flush();
        flush();
    }

}
