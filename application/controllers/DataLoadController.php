<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataLoadController extends CI_Controller {

	public function index() {
		$max = $this->AdvertisementModel->getTotalNumberOfAds();
		$advertisementId = $this->uri->segment(3);
		if ($max < $advertisementId) {
			return 0;
		} else {
			$data = [];
			$data['advertisement'] = $this->AdvertisementModel->getAdvertisement($advertisementId);
			$this->load->view('dataload', $data);
		}
	}
}
