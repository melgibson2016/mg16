<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if ($this->facebook->logged_in()) {
			// TODO: make this redirect to controller/method vms
			$this->userLoggedInFacebook();
		}
		$this->lang->load(array('login', 'top_bar', 'header'), getLanguage());
		$this->load->view('login');
		$this->session->referrerURL = $this->agent->referrer();
	}

	public function userLoggedInFacebook() {
		if ($this->facebook->logged_in()) {
			$userData = $this->facebook->user()['data'];
			$user = $this->UserModel->getUserByFacebookId($userData['id']);
			if (!$user) {
				$this->UserModel->createUser(
					explode(" ", $userData['name'])[0],
					explode(" ", $userData['name'])[1],
					lcfirst(explode(" ", $userData['name'])[0]).".".lcfirst(explode(" ", $userData['name'])[1]."@gmail.com"),
					//$userData["email"],
					$userData["id"]
				);
				$user = $this->UserModel->getUserByFacebookId($userData['id']);
			}
			$this->logInUser($user);
//			$this->userLoggedIn($user);
		}
		else {
			// todo: redirecti siia samma kontrollerisse
		}
	}

//	public function userLoggedIn($user) {
//
//	}

	public function logInUser($user) {
		$this->session->user = $user;
		$redirectUrl = ($this->session->referrerURL ? $this->session->referrerURL : $this->config->item('base_url'));
		redirect($redirectUrl);
	}

	public function logOut() {
		$this->session->user = null;
		$this->session->set_userdata(array());
		$this->facebook->destroy_session();
	}
}
