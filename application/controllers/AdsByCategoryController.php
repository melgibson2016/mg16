<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdsByCategoryController extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->helper('language');
		$this->lang->load(array('top_bar', 'welcome_text', 'header', 'category_list'), getLanguage());
		$categoryId = $this->uri->segment(3);
		$data = [];
//		$this->load->model('AdvertisementModel');
//		$this->load->model('CategoryModel');
		// TODO: autocomplete $this->Advertisement->... siia
		$data['categories'] = $this->CategoryModel->getCategories();
		$data['advertisementsInCategory'] = $this->AdvertisementModel->getAdvertisementsByCategory($categoryId);
		$data['advertisementCount'] = $this->AdvertisementModel->getAdvertisementsCountByCategory($categoryId);
//		fb($this->AdvertisementModel->getAdvertisementsCountByCategory($categoryId));
		$this->load->view('ads_by_category', $data);
	}
}
