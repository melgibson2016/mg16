<?php
$lang['tagasi'] = 'Back';
$lang['põhiinfo'] = 'Info';
$lang['hind'] = 'Price';
$lang['kogus'] = 'Quantity';
$lang['maksmine'] = 'Payment type';
$lang['asukoht'] = 'Location';
$lang['kuulutuse_autor'] = 'Advertiser';
$lang['kasutajanimi'] = 'Username';
$lang['email'] = 'E-mail';
$lang['kirjeldus'] = 'Details';
?>