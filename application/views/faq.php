<?php $this->view('partials/header'); ?>
<body>
    <?php $this->view('partials/top_bar'); ?>

    <?php $this->view('partials/welcome_text'); ?>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="content">
                    <h2 class="title"><?php echo lang('korduma_kippuvad_küsimused'); ?></h2>
                    <br>
                    <h3>Kust saaks nautida offline kasutajakogemust?</h3>
                    <p>
                        <a href="/Kasutamisleping">Vajuta siia, tõmba juhe tagant ja naudi: nii, kodus, koolis, kui ka autoroolis!</a>
                    </p>
                    <h3>Lorem ipsum?</h3>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean scelerisque tellus eu dolor sodales luctus. Pellentesque pharetra iaculis mattis. Sed laoreet tincidunt eros id aliquam. Quisque ultrices pretium rutrum. Nam suscipit, lacus vitae tristique ultricies, elit nisi euismod elit, a euismod erat tortor a quam. Nullam auctor, dui ac rhoncus commodo, mauris sapien blandit ante, vel lacinia lorem tellus eget arcu. Morbi sed purus vel metus ultrices efficitur.
                    </p>
                    <h3>Aenean nec magna dolor?</h3>
                    <p>
                        Aenean nec magna dolor. Quisque facilisis iaculis diam, ac egestas ante varius et. Nam vel magna vel nibh condimentum pretium et quis leo. Quisque et pellentesque diam. Ut varius venenatis ex, id consequat quam mattis sit amet.
                    </p>
                    <h3 id="javascript-loading"><a><?=lang('vajuta_siia'); ?></a></h3>
                </div>
            </div>

        </div>

    </div>
<?php $this->view('partials/footer'); ?>
</body>
</html>