<?php $this->view('partials/header'); ?>

<body>

<?php $this->view('partials/top_bar'); ?>
<?php $this->view('partials/welcome_text'); ?>

    <div class="container">

        <div class="content">

            <div class="row">

                <div class="col-md-12">
                    <a class="back">&lt;&lt; <?php echo lang('tagasi'); ?></a>
                    <h2 class="kuulutuse-pealkiri">Kuulutuse pealkiri</h2>
<!--                    <h2><a href="/Kuulutus/--><?//=$advertisement['advertisement_id'];?><!--/Osta">OSTA!</a></h2>-->
                    <h2><a class="osta" data-id=<?=$advertisement['advertisement_id'];?>>OSTA! </a></h2>
                    <?php if(isset($paymentSuccess)):?>
                        <h2><?=($paymentSuccess===True?'Makse õnnestus!':'Makset ei sooritatud!')?></h2>
                    <?php endif;?>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
                    <img src="../../public_files/img/250x250.jpg" alt="250x250 pilt">
                </div>

                <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12">

                    <div class="row">
                        <div class="col-md-12">
                            <h3><?php echo lang('põhiinfo'); ?></h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-5">
                            <ul class="ad-info leftmost-ad-info">
                                <li><?php echo lang('hind'); ?></li>
                                <li><?php echo lang('kogus'); ?></li>
                                <li><?php echo lang('maksmine'); ?></li>
                                <li><?php echo lang('asukoht'); ?></li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-5">
                            <ul class="ad-info">
                                <li><?=$advertisement['item_price'];?>&euro;</li>
                                <li><?=$advertisement['amount'];?> tk</li>
                                <li><?=$advertisement['payment_type'];?> <span class="glyphicon glyphicon-question-sign" title="abiinfo"></span></li>
                                <li><?=$advertisement['location'];?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h3><?php echo lang('kuulutuse_autor'); ?></h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-5">
                            <ul class="ad-info leftmost-ad-info">
                                <li><?php echo lang('kasutajanimi'); ?></li>
                                <li><?php echo lang('email'); ?></li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-5">
                            <ul class="ad-info">
                                <li><?=$advertisement['firstname'];?>&nbsp<?=$advertisement['lastname'];?></li>
                                <li><?=$advertisement['email'];?></li>
                            </ul>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3><?php echo lang('kirjeldus'); ?></h3>
                    <p><?=$advertisement['description'];?></p>
                </div>
            </div>

        </div>

    </div>
<?php $this->view('partials/footer'); ?>
<script type="text/javascript" src="../../public_files/js/ad.js"></script>
</body>
</html>