<div class="welcome">
    <div class="container">
        <h1><?php echo lang('kauple_meiega'); ?></h1>
        <p class="welcome-p"><?php echo lang('parim_ostuportaal'); ?></p>
        <?php if (!$this->session->user['id']):?>
            <button type="button" class="btn btn-lg mainpage-btn logi_sisse"><?php echo lang('logi_sisse'); ?></button>
        <?php else: ?>
            <button type="button" class="btn btn-lg mainpage-btn lisa"><?php echo lang('lisa'); ?></button>

        <?php endif; ?>
<!--        <button type="button" class="btn btn-lg mainpage-btn registreeru"><?php echo lang('registreeru'); ?></button>-->
    </div>
</div>