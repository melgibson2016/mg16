<div class="navbar navbar-default navbar-static-top">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#my-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand index">MG16</a>
        </div>

        <div class="collapse navbar-collapse" id="my-menu">
            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo lang('vali_keel'); ?><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="change-language" data-language="ee" href="#">EST</a></li>
                        <li><a class="change-language" data-language="en" href="#">ENG</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php if(0): ?>
                    <li><a>FB: <?=($this->facebook->logged_in()?$this->facebook->user()['data']['name']:"POLE")?></a></li>
                    <li><a>USER: <?=(isset($this->session->user["id"])?$this->session->user["firstname"]:"POLE")?></a></li>
                    <pre>
                        <?php print_r($this->session->user); ?>
                    </pre>
                <?php endif;?>
                <li><a class="index <?=$this->uri->segment(2)==null?'active':''?>"><?php echo lang('avalehele'); ?></a></li>
                <li><a class="kkk <?=$this->uri->segment(2)=='KKK'?'active':''?>"><?php echo lang('kkk'); ?></a></li>
                <li><a class="kontakt <?=$this->uri->segment(2)=='Kontakt'?'active':''?>"><?php echo lang('kontakt'); ?></a></li>
                <?php if (isset($this->session->user["id"])):?>
                    <li><a class="log-out-button"><?php echo lang('logi_välja'); ?></a></li>
                <?php endif; ?>
                <?php if (isset($this->session->user["id"]) && in_array($this->session->user["facebook_user_id"], array(1153424361347992, 584616051714499, 1093703340672334))):?>
                    <li><a class="admin-button">Admin</a></li>
                <?php endif; ?>
            </ul>
        </div>

    </div>
</div>