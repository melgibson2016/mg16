<h2><?php echo lang('kategooriad'); ?></h2>
<ul id="categories-list">
    <?php foreach($categories as $categoryData):?>
        <?php
            $activeClass = ($this->uri->segment(3)==$categoryData['id']?'active':'');
        ?>
        <li>
            <a class="kategooria <?=$activeClass?>" data-id="<?php echo $categoryData['id']; ?>"><?php echo lang($categoryData['category_name']); ?></a>
        </li>
    <?php endforeach; ?>
</ul>