<?php $this->view('partials/header'); ?>
<body>

    <?php $this->view('partials/top_bar'); ?>

    <?php $this->view('partials/welcome_text'); ?>

        <div class="container">

            <div class="content">

                <div class="row">

                    <div class="col-md-3">
                        <?php $this->view('partials/category_list', $categories); ?>
                    </div>

                    <div class="col-md-9">
                        <p><?php echo lang('Kokku_kuulutusi'); ?> <?=$advertisementCount?></p>

                        <div class="row">

                            <?php foreach($advertisementsInCategory as $advertisementData):?>
                                <?php $this->view('partials/advertisement_short', array('advertisement'=>$advertisementData)); ?>
                            <?php endforeach; ?>

                        </div>

                    </div>

                </div>

            </div>

        </div>

<?php $this->view('partials/footer'); ?>

</body>
</html>