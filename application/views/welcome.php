<?php $this->view('partials/header'); ?>
<body>
<?php $this->view('partials/top_bar'); ?>

<?php $this->view('partials/welcome_text'); ?>

<div class="container">

    <div class="content">

        <div class="row">
            <div class="col-md-3">
                <?php $this->view('partials/category_list', $categories); ?>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                    <img class="slide-image" src="../../public_files/img/800x300.jpg" alt="800x300 pilt">
                        <!-- <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="http://placehold.it/800x300" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div> -->
                    </div>

                </div>

                <div class="row ad">
                    <?php foreach(array_slice($advertisements, 0, 3) as $advertisementData):?>
                        <?php $this->view('partials/advertisement_short', array('advertisement'=>$advertisementData)); ?>
                    <?php endforeach; ?>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <a class="laadi-rohkem">Laadi rohkem</a>
                    </div>
                </div>

            </div>

        </div>

    </div>
</div>
<?php $this->view('partials/footer'); ?>
<!--<script type="text/javascript" src="../../public_files/js/load_more_ads.js"></script>-->
</body>
</html>