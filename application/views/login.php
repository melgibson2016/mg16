<?php $this->view('partials/header'); ?>
    <body>
    <?php $this->view('partials/facebook_script'); ?>

    <?php $this->view('partials/top_bar'); ?>

    <div class="welcome">
        <div class="container">
            <h1><?php echo lang('logi_sisse'); ?></h1>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <div class="content">
                    <div class="login-btns">
                        <p id="waiting-notification"><?php echo lang('oodake'); ?></p>
                        <button id="facebook-login-button" type="button" class="btn btn-lg facebook-login-btn"><?php echo lang('facebookiga'); ?></button><br>
<!--                        <button type="button" class="btn btn-lg idkaart-login-btn">Logi sisse ID-kaardiga</button>-->
<!--                        <p>või <a class="registreeru">registreeru</a>.</p>-->
                    </div>
                </div>
            </div>

        </div>

    </div>
<?php $this->view('partials/footer'); ?>
</body>
</html>