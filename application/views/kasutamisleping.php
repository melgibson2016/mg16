<!DOCTYPE html>
<html manifest="cache.appcache" lang="et">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width = device-width, initial-scale = 1">
    <meta charset="UTF-8">
    <title><?php echo lang('tiitel'); ?></title>
    <link rel="stylesheet" type="text/css" href=<?php echo(base_url("/public_files/css/bootstrap.min.css")); ?>>
<!--	<link rel="stylesheet" type="text/css" href="/public_files/css/quicksand.css">-->
    <link rel="stylesheet" href=<?php echo(base_url("/public_files/css/jquery-ui.css")); ?>>
    <link rel="stylesheet" type="text/css" href=<?php echo(base_url("/public_files/css/stylesheets.css")); ?>>
</head>
<body>
	<div class="content">
		<p>
			Seda lehte saad refreshida ja järgmisi ridu klikkides highlightida ka võrguta ühenduse puhul:
		</p>
		<div>
			<div data-offlineelementnumber=0 class="offlineTestElement">AAAAAAAAAAAAA</div>
			<div data-offlineelementnumber=1 class="offlineTestElement">BBBBBBBBBBBBB</div>
			<div data-offlineelementnumber=2 class="offlineTestElement">CCCCCCCCCCCCC</div>
		</div>
	</div>
	<footer>
		<div class="container">
			<div class="copyright">&copy; 2016 MG16</div>
		</div>
	</footer>
	<script type="text/javascript" src="../../public_files/js/jquery.js"></script>
	<script type="text/javascript" src="../../public_files/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../../public_files/js/skript.js"></script>
	<script type="text/javascript" src="../../public_files/js/kasutamisleping.js"></script>
</body>