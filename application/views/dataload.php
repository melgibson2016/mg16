<div class="col-sm-4 col-lg-4 col-md-4">
    <div class="thumbnail">
        <img src="../../public_files/img/320x150.jpg" alt="320x150 pilt">
        <div class="caption">
            <h3 class="index-kuulutus-pealkiri"><a class="kuulutus" data-id="<?=$advertisement['advertisement_id'];?>">Kuulutus</a></h3>
            <p><?=(strlen($advertisement['description']) > 40) ? substr($advertisement['description'], 0, 40).'...' : $advertisement['description'];?></p>
            <p class="pull-right"><?=$advertisement['item_price'];?>&euro;</p>
            <br>
        </div>
    </div>
</div>