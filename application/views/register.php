<?php $this->view('partials/header'); ?>

<body>
    <?php $this->view('partials/top_bar'); ?>

    <div class="welcome">
        <div class="container">
            <h1><?php echo lang('registreeru'); ?></h1>
        </div>
    </div>

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <div class="content">
                    <div class="login-btns">
                        <button type="button" class="btn btn-lg facebook-login-btn"><?php echo lang('facebookiga'); ?></button><br>
                        <button type="button" class="btn btn-lg idkaart-login-btn"><?php echo lang('id_kaardiga'); ?></button>
                        <p><?php echo lang('või'); ?> <a class="logi_sisse"><?php echo lang('logi_sisse'); ?></a>.</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
<?php $this->view('partials/footer'); ?>
</body>
</html>