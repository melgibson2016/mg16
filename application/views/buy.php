<?php $this->view('partials/header'); ?>

<body>

<?php $this->view('partials/top_bar'); ?>
<?php $this->view('partials/welcome_text'); ?>
<?php

// THIS IS AUTO GENERATED SCRIPT
// (c) 2011-2016 Kreata OÜ www.pangalink.net

// File encoding: UTF-8
// Check that your editor is set to use UTF-8 before using any non-ascii characters

// STEP 1. Setup private key
// =========================

$private_key = openssl_pkey_get_private(
    "-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAuq9yAIcNmHi9eNjP3khnJIYlmWeVdNwhAZG2UWDvqkpMIQZ5
7YmrJGv8baK4J5WQ4m2TmnTGbE7if/vWcjQWYRVgjjkkkRUC4U6WgmpepKcDyEZw
6TVtCkI4YsKouJdz0mH6SuqkPIBqdCzhEd7ckCZX2Ptr62UHmhroYQ0vWEqDvW4B
cT3WHLugdYzCklFWjatK5aFatWTTX04zzaee9F+SWcD7zwiaOeh7IzfduIyWV5K2
Fl1RmDSwKIP/HxQwz/tJq5NZjTPMFAP97vTOx5RfzfxCQ2rjqZe6znXnqN3+Is4q
MoKITY/agar65quH0xoJUg7UP05J9kjmE6XAIwIDAQABAoIBAQCmWXu+WJmWydAc
BSaFLgMqmpaacaDN9vE6UrshHL2zAm1/6CNxtQDZfZf7oclzDGWEPpwOQAwnqstN
u/zE2wEuFTxqvySEWzKrzbl0EJCxK1nFdm9af47gW02fQ1HQ2rQMPd4oL5mVh+BU
EUGlCO+FS4VwBemOYgI6Icv73yHXC1k7W6j6GfJQ3FWd+lafp1gwazh7HhicKiuq
FD1z4nEIj2cPctat0Y332upgpaUDB6aryknwzub5smuTcaATj1Eb+yrGM7mTUehU
V/vvVNRzKqeUVtFiu0QrpZJTpceK+fwOZzeeI2VNpBAndnG30Yfd4uBZKyhUVBRv
FE7XsLzxAoGBAOFrTFkWGgVIg8/noQhwgthHuA0OyCRN9LFV839kCKXa6WZs1EYe
/kp393VKTEDTUvVhM264iHGmmIzzxnGUGuRZMnqczhKQp2UDEqhB837Edg+udJm4
y9BMTtL3zknoFfepsCfOUxwhLdxS/hU7tyr54MIkaXZr7p+FVIughDk9AoGBANQC
8LCrJ/g8KWhepaV8HHiuQAFTM/3aYCIWinsAwRnA89qxCyiOOSpktq7yUco4VCAO
spRJfwZuHmX4dYoVU5CteXFar3//Gh7q/vVBMpGi6Jv3PeqxlLnjip4ZX9dDl+e3
HWeABrEJdMYL7W3Q4r3WGWqLv7b+HPN8f3mirbTfAoGAEsUNcWmOtxvnpdsipIJB
0uJ959BwR92Z315JzTS2J6FHx/iN4FzaTbcZitGp8cfm43m5f6Gr8tKtZycxQenU
J2lp85205wTWe2m7FvmO3eJb9khPezFQ/0IrWhO6QJsroRinC1l5wCDfIbZjY786
LEDsPx4rDepEz1GZzZqLB/0CgYAaxdRHOxXtDwvbljgs0aN2yepjQHKMWAtTwqzy
I4I/ElK5Byp1kKA/bkBzjz4s3zggZDrA5xcXZMiJ7/pH2bGXz5BKdD09WKWfyeUw
a29LXNsq9unmvU4eu1FB7cpgkLSaGlGoRE6UBLGuYMs21u4LMwI6pjMrlKNrTAl8
/sIY2QKBgGdw1BuzLuAtns4R7d85bfQsrU1s6mimVzWz7kHYflUrFhxN7GjSyDoP
p7XusYZhRAAvnrxxXxyxPGCr5dHNJmk3rjsvVZ8r7555UOUzrNUURo90uUFSU5KR
hGi1YXAGAHsNOW70vKvZhYD9Ra3s+fPHAc/g4mqmGG/7xO5AZAVv
-----END RSA PRIVATE KEY-----");

// STEP 2. Define payment information
// ==================================

$fields = array(
    "VK_SERVICE"     => "1011",
    "VK_VERSION"     => "008",
    "VK_SND_ID"      => "uid100010",
    "VK_STAMP"       => "12345",
    "VK_AMOUNT"      => $advertisement['item_price'],
    "VK_CURR"        => "EUR",
    "VK_ACC"         => "EE871600161234567892",
    "VK_NAME"        => $advertisement['firstname']. ' ' .$advertisement['lastname'],
    "VK_REF"         => "1234561",
    "VK_LANG"        => "EST",
    "VK_MSG"         => "Kuulutus MG16 portaalis",
    "VK_RETURN"      => $this->config->item('base_url')."/Kuulutus/".$advertisement['advertisement_id']."/MakseOnnestus",
    "VK_CANCEL"      => $this->config->item('base_url')."/Kuulutus/".$advertisement['advertisement_id']."/MakseNurjus",
    "VK_DATETIME"    => "2016-04-10T19:25:28+0300",
    "VK_ENCODING"    => "utf-8",
);

// STEP 3. Generate data to be signed
// ==================================

// Data to be signed is in the form of XXXYYYYY where XXX is 3 char
// zero padded length of the value and YYY the value itself
// NB! Ipizza Testpank expects symbol count, not byte count with UTF-8,
// so use `mb_strlen` instead of `strlen` to detect the length of a string

$data = str_pad (mb_strlen($fields["VK_SERVICE"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_SERVICE"] .    /* 1011 */
    str_pad (mb_strlen($fields["VK_VERSION"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_VERSION"] .    /* 008 */
    str_pad (mb_strlen($fields["VK_SND_ID"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_SND_ID"] .     /* uid100010 */
    str_pad (mb_strlen($fields["VK_STAMP"], "UTF-8"),   3, "0", STR_PAD_LEFT) . $fields["VK_STAMP"] .      /* 12345 */
    str_pad (mb_strlen($fields["VK_AMOUNT"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_AMOUNT"] .     /* 150 */
    str_pad (mb_strlen($fields["VK_CURR"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_CURR"] .       /* EUR */
    str_pad (mb_strlen($fields["VK_ACC"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_ACC"] .        /* EE871600161234567892 */
    str_pad (mb_strlen($fields["VK_NAME"], "UTF-8"),    3, "0", STR_PAD_LEFT) . $fields["VK_NAME"] .       /* ÕIE MÄGER */
    str_pad (mb_strlen($fields["VK_REF"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_REF"] .        /* 1234561 */
    str_pad (mb_strlen($fields["VK_MSG"], "UTF-8"),     3, "0", STR_PAD_LEFT) . $fields["VK_MSG"] .        /* Torso Tiger */
    str_pad (mb_strlen($fields["VK_RETURN"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_RETURN"] .     /* http://localhost:8080/project/NigTUMHuV0q1Gx36?payment_action=success */
    str_pad (mb_strlen($fields["VK_CANCEL"], "UTF-8"),  3, "0", STR_PAD_LEFT) . $fields["VK_CANCEL"] .     /* http://localhost:8080/project/NigTUMHuV0q1Gx36?payment_action=cancel */
    str_pad (mb_strlen($fields["VK_DATETIME"], "UTF-8"), 3, "0", STR_PAD_LEFT) . $fields["VK_DATETIME"];    /* 2016-04-10T19:25:28+0300 */

/* $data = "0041011003008009uid10001000512345003150003EUR020EE871600161234567892009ÕIE MÄGER0071234561011Torso Tiger069http://localhost:8080/project/NigTUMHuV0q1Gx36?payment_action=success068http://localhost:8080/project/NigTUMHuV0q1Gx36?payment_action=cancel0242016-04-10T19:25:28+0300"; */

// STEP 4. Sign the data with RSA-SHA1 to generate MAC code
// ========================================================

openssl_sign ($data, $signature, $private_key, OPENSSL_ALGO_SHA1);

/* GIx6vMm+UpEgnNvUT5yEiw90XKCeG1W+Kb6tsuqEZ6ylnzHKOmJjM9t3h013Z1W1Mt+a6DKsIxt50hLIOfbXbSmPCDSuPO9wTKurIQtvpRUCq53mERwOwZ28PwjIv8+NRUq/PXosbNqPEyVOQfmC1d3flLBum50J1Q1aX8N1pM9iUQanSNpVyzuWgf62zs4NoUf0N7JhGpWD5N3a9AYJG+d4m72opaA18KFru61RmHAej12dkYUUEchuER415HNYFbwaoD3Y4d6g1q8BiIZjGvJs0f8vFbihbvMaDUj2vBXaBvL+FHDtOvjL6Fe78CeOfguOg71f9wxtLHNXfQ/dig== */
$fields["VK_MAC"] = base64_encode($signature);

// STEP 5. Generate POST form with payment data that will be sent to the bank
// ==========================================================================
?>
<div class="container">

    <div class="content">

        <div class="row">
    <h1><a href="http://localhost:8080/">Pangalink-net</a></h1>
    <p>Makse teostamise näidisrakendus <strong>"ipizza_nimetus"</strong></p>

    <form method="post" action="http://localhost:8080/banklink/ipizza">
        <!-- include all values as hidden form fields -->
        <?php foreach($fields as $key => $val):?>
            <input type="hidden" name="<?php echo $key; ?>" value="<?php echo htmlspecialchars($val); ?>" />
        <?php endforeach; ?>

        <table>
            <tr>
                <td>Saaja</td>
                <td><?=$fields['VK_NAME']?></td>
            </tr>
            <tr>
                <td>Summa</td>
                <td><?=$fields['VK_AMOUNT'] . ' ' .$fields['VK_CURR']?></td>
            </tr>
            <!-- when the user clicks "Edasi panga lehele" form data is sent to the bank -->
            <tr><td colspan="2"><input type="submit" value="Edasi panga lehele" /></td></tr>
        </table>
    </form>
</div>
</div>
</div>

<?php $this->view('partials/footer'); ?>
</body>
</html>