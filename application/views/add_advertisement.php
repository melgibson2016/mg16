<?php $this->view('partials/header'); ?>
<body>

<?php $this->view('partials/top_bar'); ?>
<?php $this->view('partials/welcome_text'); ?>

<div class="container">

    <div class="content">

        <div class="row">

            <div class="col-md-12">
                <a class="back">&lt;&lt; <?php echo lang('tagasi'); ?></a>
                <h2 class="kuulutuse-pealkiri">Kuulutuse Lisamine</h2>
            </div>

        </div>

        <form action="Lisa" method="get">
            <table>
            <tbody>
            <tr>
                <td><label for="pealkiri"><?php echo lang('kuulutuse_pealkiri'); ?></label></td>
                <td><input id="pealkiri" name="pealkiri" type="text"></td>
            </tr>

            <tr>
                <td><label for="hind"><?php echo lang('hind'); ?></label></td>
                <td><input id="hind" name="hind" type="text" size="4"> €</td>
            </tr>

            <tr>
                <td><label for="kogus"><?php echo lang('kogus'); ?></label></td>
                <td><input id="kogus" name="kogus" type="text" size="4"> tk</td>
            </tr>

            <tr>
                <td><label for="maksmine"><?php echo lang('maksmine'); ?></label></td>
                <td><select name="maksmine" id="maksmine">
                    <option value="sularaha">sularaha</option>
                    <option value="ülekanne">ülekanne</option>
                </select></td>
            </tr>

            <tr>
                <td><label for="asukoht"><?php echo lang('asukoht'); ?></label></td>
                <td><input id="asukoht" name="asukoht" type="text"></td>
            </tr>

            <tr>
                <td><label for="kirjeldus"><?php echo lang('kirjeldus'); ?></label></td>
                <td><textarea id="kirjeldus" name="kirjeldus" rows="4" cols="60"></textarea></td>
            </tr>

            <tr>
                <td><label for="kategooria"><?php echo lang('kategooria'); ?></label></td>
                <td><select id="kategooria" name="kategooria">
                    <?php foreach($categories as $categoryData):?>
                        <option value="<?php echo $categoryData['id']; ?>"><?php echo lang($categoryData['category_name']); ?></option>
                    <?php endforeach; ?>


                </select></td>
            </tr>

            <tr>
                <td><input type="submit" value="Add"></td>
            </tr>
                </tbody>
            </table>
        </form>
    </div>

</div>
<?php $this->view('partials/footer'); ?>
<script type="text/javascript" src="../../public_files/js/ad.js"></script>
</body>
</html>