<?php $this->view('partials/header'); ?>
<body>

    <?php $this->view('partials/top_bar'); ?>

    <?php $this->view('partials/welcome_text'); ?>

    <div class="container">

        <div class="row">

            <div class="col-lg-12">
                <div class="content">
                    <h2 class="title"><?php echo lang('kontakt'); ?></h2>
                    <h3><?php echo lang('kes_me_oleme'); ?></h3>
                    <p>
                        <?php echo lang('me_oleme'); ?>
                        bla bla bla lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean scelerisque tellus eu dolor sodales luctus. Pellentesque pharetra iaculis mattis.
                    </p>

                    <h3><?php echo lang('meie_tiim'); ?></h3>
                    <div id="tiim">
                        <script type="text/javascript" src="../../public_files/js/load_XML.js"></script>
                    </div>

                    <h3><?php echo lang('kus_me_asume'); ?></h3>
                    <div class="google-maps">
                        <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
                        <script type="text/javascript" src="../../public_files/js/load_map.js"></script>
                        <div id="maps">
                            <div id='gmap_canvas'></div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
<?php $this->view('partials/footer'); ?>
</body>
</html>