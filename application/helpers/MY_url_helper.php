<?php

/*function base_url($uri)
{
    $currentInstance =& get_instance();

    $keycdnUrl = $currentInstance->config->item('keycdn_url');
    $extensions = array('css', 'js', 'jpg', 'jpeg', 'png', 'gif','pdf');
    $pathParts = pathinfo($uri);

//    if (!empty($keycdnUrl) && in_array($pathParts['extension'], $extensions)) {
//        return  $keycdnUrl . $uri;
//    }

    return $currentInstance->config->base_url($uri);
}*/

function getLanguage() {
    $currentInstance =& get_instance();
    $language = $currentInstance->uri->segment(1);
    if (!in_array($language, array('en','ee')))
        $language = 'ee';
    return $language;
}