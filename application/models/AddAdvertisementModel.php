<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdvertisementModel extends CI_Model {

    /*
     * (JOIN) lause on kirjutatud minu poolt, aga siin on see antud k�su "show create view advertisement_view" v�ljundina:
     *
     'CREATE VIEW `advertisement_view` AS select `ad`.`id` AS `advertisement_id`,`ad`.`advertiser_user_id` AS `advertiser_user_id`,`ad`.`item_price` AS `item_price`,`ad`.`amount` AS `amount`,`ad`.`payment_type` AS `payment_type`,`ad`.`location` AS `location`,`ad`.`description` AS `description`,`ad`.`category_id` AS `category_id`,`c`.`category_name` AS `category_name`,`u`.`firstname` AS `firstname`,`u`.`lastname` AS `lastname`,`u`.`email` AS `email`,`u`.`username` AS `username` from ((`advertisement` `ad` left join `category` `c` on((`ad`.`category_id` = `c`.`id`))) left join `user` `u` on((`ad`.`advertiser_user_id` = `u`.`id`)))'
     * */

    public function getAdvertisementsByCategory($categoryId) {
        $sql = "SELECT * FROM advertisement_view WHERE category_id = ?";
        $query = $this->db->query($sql, $categoryId);
        return $query->result_array();
    }

    public function getAllAdvertisements() {
        $sql = "SELECT * FROM advertisement_view";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function getAdvertisement($advertisementId) {
        $sql = "SELECT * FROM advertisement_view WHERE advertisement_id = ?";
        $query = $this->db->query($sql, $advertisementId);
        return $query->result_array()[0];
    }

    public function getAdvertisementsCountByCategory($categoryId) {
        $sql = "SELECT COUNT(*) AS count FROM advertisement_view WHERE category_id = ?";
        $query = $this->db->query($sql, $categoryId);
        return $query->result_array()[0]['count'];
    }

    public function getTotalNumberOfAds() {
        $sql = "SELECT COUNT(*) AS count FROM advertisement_view";
        $query = $this->db->query($sql);
        return $query->result_array()[0]['count'];
    }


}