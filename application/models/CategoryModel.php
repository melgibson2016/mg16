<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CategoryModel extends CI_Model {

    public $categoryName;
    public $categoryId;

    public function getCategories(){

        /* @var $query CI_DB_result */
        $query = $this->db->query('SELECT * FROM category');
        return $query->result_array();
    }
}