<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DataPushMessageModel extends CI_Model {

    public $messageId;
    public $messageText;

    public function getMessage(){
        /* @var $query CI_DB_result */
        $query = $this->db->query('SELECT * FROM DataPushMessages LIMIT 1');
        return $query->result_array();
    }

    public function getFirstMessageAfterTimestamp($unixTimestamp){
        /* @var $query CI_DB_result */
        //todo: add parameter to query string
        $queryString = 'SELECT * FROM DataPushMessages WHERE '. round($unixTimestamp) . ' < message_timestamp ORDER BY message_timestamp ASC LIMIT 1';
        $query = $this->db->query($queryString);
        $result = $query->result_array();
        if (isset($result[0]))
            return $result[0];
        else
            return null;
    }

    public function createMessage($messageText) {
        $messageTimestamp = round(microtime(true));
        $data = array('message_text' => $messageText, 'message_timestamp' => $messageTimestamp);
        $createUserSql = $this->db->insert_string('DataPushMessages', $data);
        //  INSERT INTO `user` (`firstname`, `lastname`, `email`, `facebook_user_id`) VALUES ('Mihkel', 'Vilism�e', 'loverboy14@rate.ee', '12345678912345')
        $this->db->query($createUserSql);
    }

    public function removeMessage($messageId) {
        $this->db->where('id', $messageId);
        $this->db->delete('DataPushMessages');
    }
}