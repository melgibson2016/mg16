<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model {

//    public $firstname;
//    public $lastname;
//    public $email;
//    public $facebookUserId;

    public function getUserByFacebookId($facebookId) {
        $sql = "SELECT * FROM user WHERE facebook_user_id = ?";
        $query = $this->db->query($sql, $facebookId);
        return ($query->result_array()?$query->result_array()[0]:null);
    }

    public function createUser($firstname, $lastname, $email, $facebookUserId) {
        $data = array('firstname' => $firstname, 'lastname' => $lastname, 'email' => $email, 'facebook_user_id' => $facebookUserId);
        $createUserSql = $this->db->insert_string('user', $data);
        //  INSERT INTO `user` (`firstname`, `lastname`, `email`, `facebook_user_id`) VALUES ('Mihkel', 'Vilism�e', 'loverboy14@rate.ee', '12345678912345')
        $this->db->query($createUserSql);
    }
}