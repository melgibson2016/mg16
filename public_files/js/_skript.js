function initButtons(buttons) {
	function createCallback(location){
		return function(){
			window.location=getLanguage()+location;
		}
	}
	for (i in buttons) {
		var button = buttons[i][0];
		var location = buttons[i][1];
		$(button).click(createCallback(location));
	}
}

var buttons = [
	[".logi_sisse", "/Logi_sisse"],
	[".registreeru", "/Registreeru"],
	[".kontakt", "/Kontakt"],
	[".kkk", "/KKK"],
	[".index", "/"]
];
initButtons(buttons);

$(document).on("click", ".kuulutus", function() {
	var id = $(this).data('id');
	window.location=getLanguage()+'/Kuulutus/'+id
});

$('.osta').click(function() {
	var id = $(this).data('id');
	window.location=getLanguage()+'/Kuulutus/'+id+'/Osta'
});

$('.kategooria').click(function() {
	var id = $(this).data('id');
	window.location=getLanguage()+'/Kategooria/'+id
});

$('.back').click(function() {
	parent.history.back();
	return false;
});

$('.change-language').click(function() {
	//$.post("/Vaheta_keelt", {'language': $(this).data("language")}, function(data, status){
	//	location.reload();
	//});
	var language = $(this).data("language");
	var urlSplit = window.location.pathname.split('/');
	urlSplit[1] = language;
	window.location = urlSplit.join('/');

});

$('.log-out-button').click(function() {
	$.post(getLanguage()+"/Logi_valja", function(data, status){
		window.location=getLanguage()+'/';
	});
});

$('#javascript-loading').click(function() {
	$(this).append(' +1p');
});

$(document).on("click", ".facebook-login-btn", function() {
	FB.login(function() {
		window.location = getLanguage() + "/Logi_sisse";
	});
});

function getLanguage() {
	var language = window.location.pathname.split('/')[1];
	if (['en', 'ee'].indexOf(language)==-1)
		language = 'ee';
	return '/'+language;
}

/**
 * AJAX long-polling
 *
 * 1. sends a request to the server (without a timestamp parameter)
 * 2. waits for an answer from server.php (which can take forever)
 * 3. if server.php responds (whenever), put data_from_file into #response
 * 4. and call the function again
 *
 * @param timestamp
 */
var dataPushEnabled = true;
var dataPushRequest;
var latestMessageTimestamp;
function initDataPush(timestamp)
{
	var queryString = {'timestamp' : latestMessageTimestamp};

	dataPushRequest = $.ajax(
		{
			type: 'POST',
			url: '/DataPush',
			data: queryString,
			success: function(DataJSON){
				
				var response = jQuery.parseJSON(DataJSON);

				// message variable is set when a user sends a message
				if (message != response.messageText)
					alert("Server ütleb: " + response.messageText);

				latestMessageTimestamp = response.messageTimestamp;
				initDataPush(response.messageTimestamp);
			},
			error: function() {
				//alert('error');
				initDataPush(latestMessageTimestamp);
			}
		}
	);
}

// initialize jQuery
$(function() {
	//if (dataPushEnabled)
	//	initDataPush();
});

var message;
$('.admin-button').click(function(){
	message = prompt("Message users:");
	if (message != null) {
		//if (source)
		//	source.close();
		if (dataPushRequest) {
			//alert('abort!');
			//dataPushRequest.abort();
		}
		$.post("/AdminMessage", {'message': message}, function(data, status){
			alert(data);
		});
	}
});


// LOAD MORE ADS:


function getAdFromDB(adId) {
	$.ajax({
		url: getLanguage()+"/DataLoad/" + adId,
		type: 'GET',
		success: function(data) {
			if (data == 0) {
				adsLeft = 0;
				$(".laadi-rohkem").remove();
			} else {
				$(".ad").append(data);
			}
		}
	});
}

var adCount = 4;
var adsLeft = 1; // in case there are no ads left: adsLeft == 0

function loadAds() {
	if (adsLeft) {
		getAdFromDB(adCount);
		adCount++;
	} if (adsLeft) {
		getAdFromDB(adCount);
		adCount++;
	} if (adsLeft) {
		getAdFromDB(adCount);
		adCount++;
	}
}

$(document).ready(function () {
	var hash = document.location.hash;
	if (hash) {
		hash = hash.substr(1); // 6, 9, 12, ...
		while (adCount <= hash) {
			loadAds();
		}
	}
});

$(".laadi-rohkem").click(function() {
	loadAds();

	stateData = {
		path: window.location.href,
		scrollTop: $(window).scrollTop()
	};
	window.history.replaceState(stateData, 'title', null);
	document.location.hash = adCount - 1;

	//load new page:
	stateData = {
		path: window.location.href,
		scrollTop: $(window).scrollTop()
	};
	window.history.pushState(stateData, 'title', null);
	document.location.hash = adCount - 1;
});

$(window).on('popstate', function(e) {
	var stateData = e.originalEvent.state;
	if (stateData) {
		$(window).scrollTop(stateData.scrollTop);
	}
});

// LOAD_MORE_ADS end

