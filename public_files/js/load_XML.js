
function parseXML() {
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/authors.xml', true);
	xhr.send();
	xhr.onreadystatechange = function() {
		if (xhr.readyState != 4) return;
		if (xhr.status != 200) {
			console.log(xhr.status + ': ' + xhr.statusText);
		} else {
			document.getElementById('tiim').innerHTML = "";
			var xml_doc = xhr.responseXML;
			var autorid = xml_doc.getElementsByTagName("autor");
			for (var i = 0; i < autorid.length; i++) {
				var autor = autorid[i];

				var nimi = autor.getElementsByTagNameNS("http://www.autorid.ee/xml/autor", "nimi")[0];
				document.getElementById('tiim').innerHTML += "<p>";
				document.getElementById('tiim').innerHTML += "<b>Nimi: </b>" + nimi.textContent.toString() + "<br/>";
				document.getElementById('tiim').innerHTML += "<b>Keeled ja tehnoloogiad: </b>";

				var tehnoloogiad = autor.getElementsByTagName("tehnoloogia");
				for (var j = 0; j < tehnoloogiad.length; j++) {
					var tehnoloogia = tehnoloogiad[j].getElementsByTagNameNS("http://www.tehnoloogiad.ee/xml/tehnoloogia", "nimi")[0];
					var kogemus = tehnoloogiad[j].getElementsByTagName("kogemus")[0];
					document.getElementById('tiim').innerHTML += tehnoloogia.textContent.toString() + " (" + kogemus.textContent.toString() + ")";
					if (j != tehnoloogiad.length - 1) document.getElementById('tiim').innerHTML += ", ";
				}

				document.getElementById('tiim').innerHTML += "</ul>";
				document.getElementById('tiim').innerHTML += "</p>";
			}
		}
	};
}

parseXML();
