$(document).on("click", ".offlineTestElement", function() {
	var newValue = !getOfflineData()[$(this).data('offlineelementnumber')];
	setOfflineData($(this).data('offlineelementnumber'), newValue);
	applyOfflineData();
});

function initOfflinePage() {
	if (typeof localStorage["data"] == "undefined") {
		localStorage["data"] = JSON.stringify([false, false, false]);
	}
	applyOfflineData();
}

function getOfflineData() {
	return JSON.parse(localStorage["data"]);
}

function setOfflineData(elementNumber, value) {
	var oldData = getOfflineData();
	oldData[elementNumber] = value;
	localStorage["data"] = JSON.stringify(oldData);
}

function applyOfflineData() {
	var data = getOfflineData();
	for (i = 0; i < 3; i++) {
		var element = $('.offlineTestElement[data-offlineelementnumber='+i+']');
		if (data[i])
			element.addClass('active');
		else
			element.removeClass('active');
	}
}

//http://stackoverflow.com/questions/5727306/html5-offline-application-cache-error-event-manifest-fetch-failed-1
$(document).ready(function () {
	//register event to cache site for offline use
	cache = window.applicationCache;
	cache.addEventListener('updateready', cacheUpdatereadyListener, false);
	cache.addEventListener('error', cacheErrorListener, false);
	function cacheUpdatereadyListener (){
		console.log('cacheUpdate!');
		window.applicationCache.update();
		window.applicationCache.swapCache();
	}
	function cacheErrorListener() {
		//alert('site not availble offline')
	}
});

dataPushEnabled = false;
initOfflinePage();